// Za združljivost razvoja na lokalnem računalniku ali v Cloud9 okolju
if (!process.env.PORT) {
  process.env.PORT = 8080;
}

// Priprava povezave na podatkovno bazo
var sqlite3 = require("sqlite3").verbose();
var pb = new sqlite3.Database("Chinook.sl3");

// Priprava strežnika
var express = require("express");
var streznik = express();
streznik.set("view engine", "ejs");
streznik.use(express.static("public"));

// Podpora sejam na strežniku
var expressSession = require("express-session");
streznik.use(
  expressSession({
    secret: "123456789QWERTY",    // Skrivni ključ za podpisovanje piškotov
    saveUninitialized: true,      // Novo sejo shranimo
    resave: false,                // Ne zahtevamo ponovnega shranjevanja
    cookie: {
      maxAge: 60000               // Seja poteče po 60 s (1 min) neaktivnost
    }
  })
);

var razmerje_USD_EUR = 0.89;

// Prikaz seznama pesmi na strani
streznik.get("/", function(zahteva, odgovor) {
  pb.all(
    "SELECT   Track.TrackId AS id, \
              Track.Name AS pesem, \
              Artist.Name AS izvajalec, \
              Track.UnitPrice * " + razmerje_USD_EUR + " AS cena, \
              COUNT(InvoiceLine.InvoiceId) AS steviloProdaj \
    FROM      Track, Album, Artist, InvoiceLine \
    WHERE     Track.AlbumId = Album.AlbumId AND \
              Artist.ArtistId = Album.ArtistId AND \
              InvoiceLine.TrackId = Track.TrackId \
    GROUP BY  Track.TrackId \
    ORDER BY  steviloProdaj DESC, pesem ASC \
    LIMIT     100",
    function(napaka, vrstice) {
      if (napaka) {
        odgovor.sendStatus(500);
      } else {
        odgovor.render("seznam", {seznamPesmi: vrstice});
      }
    }
  );
});

// Dodajanje oz. brisanje pesmi iz košarice
streznik.get("/kosarica/:idPesmi", function(zahteva, odgovor) {
  var idPesmi = parseInt(zahteva.params.idPesmi, 10);
  if (!zahteva.session.kosarica) {
    zahteva.session.kosarica = [];
  }
  // Če je pesem v košarici, jo izbrišemo
  if (zahteva.session.kosarica.indexOf(idPesmi) > -1) {
    zahteva.session.kosarica.splice(zahteva.session.kosarica.indexOf(idPesmi), 1);
  // Če pesmi ni v košarici, jo dodamo
  } else {
    zahteva.session.kosarica.push(idPesmi);
  }
  // V odgovoru vrnemo vsebino celotne košarice
  odgovor.send(zahteva.session.kosarica);
});

// Vrni podrobnosti pesmi v košarici iz podatkovne baze
var pesmiIzKosarice = function(zahteva, povratniKlic) {
  // Če je košarica prazna
  if (!zahteva.session.kosarica || zahteva.session.kosarica.length == 0) {
    povratniKlic([]);
  } else {
    pb.all(
      "SELECT Track.TrackId AS stevilkaArtikla, \
              1 AS kolicina, \
              Track.Name || ' (' || Artist.Name || ')' AS opisArtikla, \
              Track.UnitPrice * " + razmerje_USD_EUR + " AS cena, \
              0 AS popust \
      FROM    Track, Album, Artist \
      WHERE   Track.AlbumId = Album.AlbumId AND \
              Artist.ArtistId = Album.ArtistId AND \
              Track.TrackId IN (" + zahteva.session.kosarica.join(",") + ")",
      function(napaka, vrstice) {
        if (napaka) {
          povratniKlic(false);
        } else {
          povratniKlic(vrstice);
        }
      }
    );
  }
};

streznik.get("/kosarica", function(zahteva, odgovor) {
  pesmiIzKosarice(zahteva, function(pesmi) {
    if (!pesmi)
      odgovor.sendStatus(500);
    else
      odgovor.send(pesmi);
  });
});

streznik.listen(process.env.PORT, function() {
  console.log("Strežnik je pognan!");
});